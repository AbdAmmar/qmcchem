#!/bin/bash -x

set -u
set -e

cd .. ; QMCCHEM_PATH="$PWD" ; cd -
PACKAGES="cryptokit ocamlbuild getopt ocamlfind sexplib ppx_sexp_conv ppx_deriving"

# return 0 if program version is equal or greater than check version
check_version () {
    if [[ $1 == $2 ]]
    then
        return 0
    fi
    local IFS=.
    local i ver1=($1) ver2=($2)
    # fill empty fields in ver1 with zeros
    for ((i=${#ver1[@]}; i<${#ver2[@]}; i++))
    do
        ver1[i]=0
    done
    for ((i=0; i<${#ver1[@]}; i++))
    do
        if [[ -z ${ver2[i]} ]]
        then
            # fill empty fields in ver2 with zeros
            ver2[i]=0
        fi
        if ((10#${ver1[i]} > 10#${ver2[i]}))
        then
            return 1
        fi
        if ((10#${ver1[i]} < 10#${ver2[i]}))
        then
            return 0
        fi
    done
    return 0
}


i=$(gcc -dumpversion)

check_version 4.6 $i
if [[ $? == 1 ]]
then
   echo "GCC version $(gcc -dumpversion) too old. GCC >= 4.6 required."
   exit 1
fi

set +u
source "${QMCCHEM_PATH}"/qmcchemrc
set -u
cd Downloads 
chmod +x opam_installer.sh
rm --force ${QMCCHEM_PATH}/bin/opam
export OPAMROOT=${OPAMROOT:-${HOME}/.opam}


if [[ -d "$OPAMROOT" ]]
then
  set +e
  set +u
  source "${OPAMROOT}"/opam-init/init.sh 
  set -e
  set -u
  rm -f ${QMCCHEM_PATH}/bin/opam 
  ln -s $(which opam) ${QMCCHEM_PATH}/bin/opam 
fi


cat << EOF | bash ./opam_installer.sh --no-backup
${QMCCHEM_PATH}/bin/


EOF

if [[ ! -f "${QMCCHEM_PATH}"/bin/opam ]]
then
   echo "Installation of OPAM failed"
   exit 2
fi

source ${OPAMROOT}/opam-init/init.sh > /dev/null 2> /dev/null || true
${QMCCHEM_PATH}/bin/opam init --verbose --yes --compiler=4.10.0 --disable-sandboxing
touch "${QMCCHEM_PATH}"/bin/opam
eval $(${QMCCHEM_PATH}/bin/opam env)

set +u
export LD_LIBRARY_PATH="${QMCCHEM_PATH}/lib:${LD_LIBRARY_PATH}"
export LIBRARY_PATH="${QMCCHEM_PATH}/lib:${LIBRARY_PATH}"
export C_INCLUDE_PATH="${QMCCHEM_PATH}/lib:${C_INCLUDE_PATH}"
set -u
opam install -y --unlock-base ${PACKAGES} || exit 3
rm "${QMCCHEM_PATH}"/install/_build/ocaml.log
exit 0


