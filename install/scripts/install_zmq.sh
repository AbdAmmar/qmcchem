#!/bin/bash -x

TARGET=zmq
function _install()
{
  LIBVERSION=5
  cd .. ; QMCCHEM_PATH="$PWD" ; cd -
  set +u
  export C_INCLUDE_PATH="${C_INCLUDE_PATH}":./
  set -e
  set -u
  cd "${BUILD}"
  rm -f -- "${QMCCHEM_PATH}"/lib/libzmq.{a,so,so.$LIBVERSION} 
  ./configure --without-libsodium --enable-libunwind=no --prefix="${QMCCHEM_PATH}"
  make -j 8
  make install
  cd - 
  return 0
}

source scripts/build.sh

